# react-native-chipdna-mobile

Integration with the NMI ChipDNA Mobile SDK

## Installation

```sh
npm install react-native-chipdna-mobile
```

## Usage

```js
import ChipDNAMobile from "react-native-chipdna-mobile";

// ...

const result = await ChipDNAMobile.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
