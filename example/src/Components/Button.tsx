import React from 'react';
import { ButtonProps, Button } from 'react-native';
import colors from '../Styles/theme.json';

interface CustomButtonProps extends ButtonProps {
  color: keyof typeof colors;
}

export default function CustomButton({ color, ...props }: CustomButtonProps) {
  const c = colors[color] || colors['color-primary-500'];

  return <Button {...props} color={c} />;
}
