import * as React from 'react';

import {
  NativeEventEmitter,
  NativeModules,
  StyleSheet,
  View,
} from 'react-native';
import ChipDNAMobile, { DeviceType } from 'react-native-chipdna-mobile';
import Login from './Containers/Login';
import Connect from './Containers/Connect';
import Payment from './Containers/Payment';
import Button from './Components/Button';

export const ChipDNANativeEvent = new NativeEventEmitter(
  NativeModules.ChipDNAMobile
);
ChipDNANativeEvent.addListener('onSendMessage', (message) => {
  console.log(message);
});

export default function App() {
  const [loggedIn, setLoggedIn] = React.useState<boolean>(false);
  const [connected, setConnected] = React.useState<boolean>(false);
  const [device, setDevice] = React.useState<DeviceType | undefined>();

  const setConnection = (_device: DeviceType) => {
    setDevice(_device);
    console.log(_device);
    setConnected(true);
  };

  const getMerchantDetails = () => {
    doGetMerchantDetails();
  };

  const doGetMerchantDetails = async () => {
    const details = await ChipDNAMobile.getMerchantDetails();

    console.log(details);
  };

  return (
    <>
      {loggedIn ? (
        <View style={styles.container}>
          {connected && device ? (
            <>
              <View style={styles.payments}>
                <Payment device={device} connected={setConnected} />
              </View>
            </>
          ) : (
            <Connect connected={setConnection} />
          )}
          <Button
            title="Get Details"
            color="color-info-500"
            onPress={getMerchantDetails}
          />
        </View>
      ) : (
        <Login login={setLoggedIn} />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    flex: 1,
  },
  payments: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
});
