import * as React from 'react';

import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  NativeSyntheticEvent,
  NativeTouchEvent,
  ActivityIndicator,
} from 'react-native';
import ChipDNAMobile, {
  DeviceType,
  ChipDNAMobilePaymentStatus,
  PaymentType,
} from 'react-native-chipdna-mobile';
import SignatureCapture from 'react-native-signature-capture';
import colors from '../Styles/theme.json';
import Button from '../Components/Button';
import TextInput from '../Components/TextInput';
import { ChipDNANativeEvent } from '../App';

interface PaymentProps {
  device: DeviceType;
  connected: (connected: boolean) => void;
}

export default function Payment({ connected }: PaymentProps) {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [status, setStatus] = React.useState<string>('');
  const [amount, setAmount] = React.useState<string>('1.00');
  const [pin, setPin] = React.useState<string>('');
  const [payment, setPayment] = React.useState<PaymentType>();
  const [error, setError] = React.useState<string | undefined>();
  const signature = React.useRef<SignatureCapture>(null);

  const makePayment = (_ev: NativeSyntheticEvent<NativeTouchEvent>) => {
    const price = parseFloat(amount);

    if (price > 0) {
      setLoading(true);
      setError(undefined);
      doMakePayment(price);
    } else setError('The amount must be greater than zero');
  };

  const updateStatus = (_status: string) => {
    console.log(`Status ${_status}`);
    setStatus(_status);
  };

  const doMakePayment = async (price: number, retry: boolean = true) => {
    let onStatusUpdateSubscription;
    if (retry) {
      onStatusUpdateSubscription = ChipDNANativeEvent.addListener(
        'onStatusUpdate',
        updateStatus
      );
    }

    try {
      await ChipDNAMobile.reconnectDevice();
      const result = await ChipDNAMobile.makePayment(price * 100, 'USD');

      setPayment(result);
      setLoading(false);
    } catch (_error: any) {
      if (_error.message === 'TMSUpdateRequired') {
        await ChipDNAMobile.reconnectDevice();
        if (retry) {
          await doMakePayment(price, false);
        }
      } else {
        setError(_error.message);
        setLoading(false);
      }
    } finally {
      onStatusUpdateSubscription?.remove();
    }
  };

  const doSignature = async () => {
    if (payment) {
      setLoading(true);
      try {
        const result = await ChipDNAMobile.verifyPayment(payment, pin);

        console.log(result);
        setPayment(result);
        setLoading(false);
      } catch (e) {
        setLoading(false);
        console.log(e);
      }
    }
  };

  const testConnection = async () => {
    try {
      const result = await ChipDNAMobile.isDeviceConnected();

      connected(result);
    } catch (e) {
      console.log(e);
    }
  };

  const saveSignature = async () => {
    if (signature && signature.current !== null) {
      console.log('save');
      signature.current?.saveImage();
    }
  };

  const resetSignature = async () => {
    if (signature && signature.current !== null) {
      signature.current?.resetImage();
    }
  };

  const cancelPayment = async () => {
    try {
      await ChipDNAMobile.cancelVerification();
      setLoading(false);
      setPayment(undefined);
    } catch (e) {
      console.log(e);
      setLoading(false);
      setPayment(undefined);
    }
  };

  const handleSignature = async (sign: { encoded: string }) => {
    if (payment) {
      setLoading(true);
      try {
        const result = await ChipDNAMobile.verifyPayment(payment, sign.encoded);

        console.log(result);
        setPayment(result);
        setStatus('');
        setLoading(false);
      } catch (e) {
        setLoading(false);
        console.log(e);
      }
    }
  };

  const updateTMS = () => {
    doUpdateTMS();
  };

  const doUpdateTMS = async () => {
    await ChipDNAMobile.updateTMS();
    console.log('TMS Update', true);
  };

  const signatureProps = {
    backgroundColor: '#eeeeee',
    strokeColor: '#000000',
  };

  return payment &&
    (payment.status === ChipDNAMobilePaymentStatus.PIN_REQUIRED ||
      payment.status === ChipDNAMobilePaymentStatus.SIGNATURE_REQUIRED) ? (
    <>
      {payment.status === ChipDNAMobilePaymentStatus.PIN_REQUIRED ? (
        <View style={styles.container}>
          <View style={styles.box}>
            <TextInput value={pin} update={setPin} keyboardType="decimal-pad" />
            <Button
              title="Set pin"
              color="color-success-500"
              onPress={doSignature}
            />
            {loading && (
              <ActivityIndicator
                size="large"
                color={colors['color-info-500']}
              />
            )}
            {error && <Text style={styles.error}>{error}</Text>}
          </View>
        </View>
      ) : (
        <>
          {loading ? (
            <View style={styles.container}>
              <ActivityIndicator
                size="large"
                color={colors['color-info-500']}
              />
              {status !== '' && <Text>{status}</Text>}
            </View>
          ) : (
            <View style={styles.full}>
              <SignatureCapture
                ref={signature}
                style={styles.signature}
                showNativeButtons={false}
                onSaveEvent={handleSignature}
                showTitleLabel={true}
                {...signatureProps}
              />
              <View style={styles.actions}>
                <View style={styles.action}>
                  <Button
                    title="Cancel"
                    color="color-danger-500"
                    onPress={cancelPayment}
                  />
                </View>
                <View style={styles.action}>
                  <Button
                    title="Reset"
                    color="color-warning-500"
                    onPress={resetSignature}
                  />
                </View>
                <View style={styles.action}>
                  <Button
                    title="Approve signature"
                    color="color-success-500"
                    onPress={saveSignature}
                  />
                </View>
              </View>
            </View>
          )}
          {error && <Text style={styles.error}>{error}</Text>}
        </>
      )}
    </>
  ) : (
    <View style={styles.container}>
      <View style={styles.box}>
        <TextInput
          value={amount}
          update={setAmount}
          keyboardType="decimal-pad"
        />
        <Button
          title="Make payment"
          color="color-success-500"
          onPress={makePayment}
        />
        <Button
          title="Test connection"
          color="color-info-500"
          onPress={testConnection}
        />
        <Button title="Update TMS" color="color-info-500" onPress={updateTMS} />
        {loading && (
          <ActivityIndicator size="large" color={colors['color-info-500']} />
        )}
        {status !== '' && <Text>{status}</Text>}
        {error && <Text style={styles.error}>{error}</Text>}
        {payment?.transactionId && (
          <>
            <Text style={styles.info}>{payment.maskedPan}</Text>
            <Text style={styles.info}>{payment.transactionId}</Text>
          </>
        )}
        {payment?.receiptData && (
          <ScrollView style={styles.receipt}>
            {Object.values(payment.receiptData).map(
              (line, index) =>
                line.label !== '' &&
                line.value !== '' && (
                  <View key={index} style={styles.receiptLine}>
                    {line.label !== '' && (
                      <Text style={styles.receiptLabel}>{line.label}</Text>
                    )}
                    {line.value !== '' && (
                      <Text style={styles.receiptValue}>{line.value}</Text>
                    )}
                  </View>
                )
            )}
          </ScrollView>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  full: {
    flex: 1,
    padding: 12,
  },
  box: {
    width: 240,
  },
  signature: {
    flex: 1,
    backgroundColor: '#ff00ff',
    borderWidth: 1,
    borderColor: '#000033',
  },
  heading: {
    fontSize: 24,
    textAlign: 'center',
  },
  devices: {
    marginVertical: 10,
  },
  device: {
    marginVertical: 10,
  },
  actions: {
    flexDirection: 'row',
  },
  action: {
    margin: 4,
  },
  error: {
    color: colors['color-danger-500'],
    padding: 10,
    textAlign: 'center',
  },
  info: {
    color: colors['color-success-500'],
    padding: 10,
    textAlign: 'center',
  },
  receipt: {
    flexDirection: 'column',
  },
  receiptLine: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 4,
  },
  receiptLabel: {
    fontWeight: 'bold',
    marginRight: 8,
  },
  receiptValue: {
    fontWeight: 'normal',
  },
});
