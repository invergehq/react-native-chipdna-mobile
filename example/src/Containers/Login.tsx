import * as React from 'react';
import { PASSWORD, TEST_API_KEY, LIVE_API_KEY } from '@env';
import {
  StyleSheet,
  View,
  Text,
  NativeSyntheticEvent,
  NativeTouchEvent,
  ActivityIndicator,
} from 'react-native';
import ChipDNAMobile, {
  ChipDNAMobileEnvironment,
} from 'react-native-chipdna-mobile';
import colors from '../Styles/theme.json';
import Button from '../Components/Button';
import Switch from '../Components/Switch';
import TextInput from '../Components/TextInput';

interface LoginProps {
  login: (loggedIn: boolean) => void;
}
export default function Login({ login }: LoginProps) {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [password, setPassword] = React.useState<string>(PASSWORD);
  const [apiKey, setAPIKey] = React.useState<string>('');
  const [appId, setAppId] = React.useState<string>('ChipDNAMobileExample');
  const [test, setTest] = React.useState<boolean>(true);
  const [error, setError] = React.useState<string | undefined>();

  React.useEffect(() => {
    if (test) setAPIKey(TEST_API_KEY);
    else setAPIKey(LIVE_API_KEY);
  }, [test]);

  React.useEffect(() => {
    const isLoggedIn = async () => {
      setLoading(true);
      login(await ChipDNAMobile.loggedIn());
      setLoading(false);
    };
    isLoggedIn();
  }, [login]);

  const attemptLogin = (_ev: NativeSyntheticEvent<NativeTouchEvent>) => {
    setLoading(true);
    setError(undefined);
    doLogin();
  };

  const doLogin = async () => {
    let environment = ChipDNAMobileEnvironment.TEST;
    if (!test) environment = ChipDNAMobileEnvironment.LIVE;

    try {
      let result;

      result = await ChipDNAMobile.login(password, apiKey, environment, appId);

      console.log(result);
      setLoading(false);
      login(true);
    } catch (_error: any) {
      setError(_error.message);
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" color={colors['color-info-500']} />
      ) : (
        <View style={styles.box}>
          <Text style={styles.heading}>Login</Text>
          <TextInput
            update={setPassword}
            value={password}
            placeholder="Enter your password"
          />
          <TextInput
            update={setAPIKey}
            value={apiKey}
            placeholder="Enter your API key"
          />
          <TextInput
            update={setAppId}
            value={appId}
            placeholder="Enter your App ID"
          />
          <Switch label="Test" update={setTest} value={test} />
          <Button
            title="Log in"
            color="color-info-500"
            onPress={attemptLogin}
          />
          {error && <Text style={styles.error}>{error}</Text>}
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 240,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  error: {
    color: colors['color-danger-500'],
    padding: 10,
    textAlign: 'center',
  },
});
