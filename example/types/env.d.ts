declare module '@env' {
  export const PASSWORD: string;
  export const TEST_API_KEY: string;
  export const LIVE_API_KEY: string;
}
