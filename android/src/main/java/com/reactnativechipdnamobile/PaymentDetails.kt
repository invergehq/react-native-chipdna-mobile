package com.reactnativechipdnamobile

import com.creditcall.chipdnamobile.ChipDnaMobileSerializer
import com.creditcall.chipdnamobile.ParameterKeys
import com.creditcall.chipdnamobile.ParameterValues
import com.creditcall.chipdnamobile.Parameters
import com.facebook.react.bridge.ReadableMap
import com.facebook.react.bridge.WritableMap
import com.facebook.react.bridge.WritableNativeMap
import java.util.*
import kotlin.properties.Delegates

enum class PaymentStatus {
  UNKNOWN, APPROVED, DECLINED, PIN_REQUIRED, SIGNATURE_REQUIRED
}

class PaymentDetails() {
  private lateinit var id: String
  private var amount = 0
  private lateinit var status: PaymentStatus
  private var transactionId: String? = null
  private var customerId: String? = null
  private var cardId: String? = null
  private var maskedPan: String? = null
  private var receiptData: WritableNativeMap? = null

  constructor(i: String, a: Int, s: PaymentStatus) : this() {
    id = i
    amount = a
    status = s
  }

  constructor(paymentDetails: PaymentDetails, s: PaymentStatus) : this() {
    id = paymentDetails.id
    amount = paymentDetails.amount
    status = s
  }

  constructor(details: ReadableMap) : this() {
    id = if (details.hasKey("id")) {
      details.getString("id")!!
    } else UUID.randomUUID().toString()
    amount = if (details.hasKey("amount")) {
      details.getInt("amount")
    } else 0
    status = if (details.hasKey("status")) {
      details.getString("status")?.let { PaymentStatus.valueOf(it) }!!
    } else PaymentStatus.UNKNOWN
  }

  constructor(parameters: Parameters) : this() {
    id = parameters.getValue(ParameterKeys.UserReference)
    status = if (
      !parameters.containsKey(ParameterKeys.TransactionResult)
      || parameters.getValue(ParameterKeys.TransactionResult).equals(ParameterValues.Declined)
    ) PaymentStatus.DECLINED
    else PaymentStatus.APPROVED
    transactionId = parameters.getValue(ParameterKeys.TransactionId)
    customerId = parameters.getValue(ParameterKeys.CustomerVaultId)
    cardId = parameters.getValue(ParameterKeys.CardReference)
    amount = parameters.getValue(ParameterKeys.Amount).toInt()
    maskedPan = parameters.getValue(ParameterKeys.MaskedPan)
    if (parameters.containsKey(ParameterKeys.ReceiptData)) {
      val receipt = ChipDnaMobileSerializer.deserializeReceiptData(parameters.getValue(ParameterKeys.ReceiptData))
      val currentReceiptData = WritableNativeMap()

      receipt.forEach {
        val field = WritableNativeMap()
        field.putString("label", it.value.label)
        field.putString("value", it.value.value)
        currentReceiptData.putMap(it.key, field)
      }

      receiptData = currentReceiptData
    }
  }

  public fun toResponse():WritableNativeMap {
    val response = WritableNativeMap()

    response.putString("id", id)
    response.putString("status", status.toString())
    response.putInt("amount", amount)
    if (transactionId !== null) response.putString("transactionId", transactionId)
    if (customerId !== null) response.putString("customerId", customerId)
    if (cardId !== null) response.putString("cardId", cardId)
    if (maskedPan !== null) response.putString("maskedPan", maskedPan)
    if (receiptData !== null) response.putMap("receiptData", receiptData)

    return response
  }

  public fun getId(): String {
    return id
  }

  public fun getAmount(): Int {
    return amount
  }

  public fun getStatus(): PaymentStatus {
    return status
  }
}
