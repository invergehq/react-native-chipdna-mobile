package com.reactnativechipdnamobile

import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.os.AsyncTask
import android.util.Log
import androidx.core.app.ActivityCompat
import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule
import java.text.SimpleDateFormat
import java.util.*

class ChipDNAMobileModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
  override fun getName(): String {
    return "ChipDNAMobile"
  }

  fun log(message: String) {
    Log.d("RNChipDNAMobile", message)
    reactApplicationContext.getJSModule(
      DeviceEventManagerModule.RCTDeviceEventEmitter::class.java
    ).emit("onSendMessage", message)
  }

  fun statusUpdate(message: String) {
    reactApplicationContext.getJSModule(
      DeviceEventManagerModule.RCTDeviceEventEmitter::class.java
    ).emit("onStatusUpdate", message)
  }

  class Login(
    private val module: ChipDNAMobileModule,
    private val promise: Promise,
    private val callback: (() -> Unit)? = null
  ) : AsyncTask<String?, Void?, Parameters>() {

    override fun doInBackground(vararg params: String?): Parameters? {
      // send a request containing the entered passsword so it can be handled by ChipDnaMobile.
      val requestParameters: Parameters = Parameters()
      requestParameters.add(ParameterKeys.Password, params[0])
      return ChipDnaMobile.initialize(module.reactApplicationContext, requestParameters)
    }

    override fun onPostExecute(response: com.creditcall.chipdnamobile.Parameters) {
      if (response.containsKey(ParameterKeys.Result) && response.getValue(ParameterKeys.Result).equals("True", ignoreCase = true)) {
        module.log("ChipDna Mobile initialised")
        module.log("Version: " + ChipDnaMobile.getSoftwareVersion() + ", Name: " + ChipDnaMobile.getSoftwareVersionName())
        callback?.invoke()
      } else {
        // The password is incorrect, ChipDnaMobile cannot initialise
        module.log("Failed to initialise ChipDna Mobile")
        if (response.getValue(ParameterKeys.RemainingAttempts).equals("0", ignoreCase = true)) {
          // If all password attempts have been used, the database is deleted and a new password is required.
          module.log("Reached password attempt limit")
          promise.reject("Password", "Reached password attempt limit")
        } else {
          module.log("Password attempts remaining: " + response.getValue(ParameterKeys.RemainingAttempts))
          promise.reject("Password", "Password attempts remaining: " + response.getValue(ParameterKeys.RemainingAttempts))
        }
      }
    }
  }

  class Connect(
    private val module: ChipDNAMobileModule,
    private val promise: Promise,
    private val callback: (() -> Unit)? = null
  ) : AsyncTask<String?, Void?, Parameters>() {

    override fun doInBackground(vararg params: String?): Parameters? {
      val requestParameters = Parameters()
      requestParameters.add(ParameterKeys.PinPadName, params[0])
      requestParameters.add(ParameterKeys.PinPadConnectionType, params[1])
      return ChipDnaMobile.getInstance().setProperties(requestParameters)
    }

    override fun onPostExecute(response: Parameters) {
      if (
        response.containsKey(ParameterKeys.Result)
        && response.getValue(ParameterKeys.Result).equals(ParameterValues.TRUE)
      ) {
        callback?.invoke()
      } else {
        promise.reject("Device", "Failed connecting")
      }
    }
  }

  class VerifySignature(
    private val module: ChipDNAMobileModule,
    private val promise: Promise,
    private val paymentDetails: PaymentDetails,
    private val callback: (() -> Unit)? = null
  ) : AsyncTask<String?, Void?, Parameters>() {

    override fun doInBackground(vararg params: String?): Parameters? {
      val requestParameters = Parameters()
      module.log("VerifySignature")
      ChipDnaMobile.getInstance().clearAllSignatureVerificationListener()
      ChipDnaMobile.getInstance().addSignatureVerificationListener(
        SignatureVerificationListener(
          module, promise, paymentDetails
        ) {
          module.log("VerifySignature: Signature verification callback")
          val approveSignatureParameters = Parameters()
          approveSignatureParameters.add(ParameterKeys.Result, ParameterValues.TRUE)

          val response = ChipDnaMobile.getInstance().continueSignatureVerification(approveSignatureParameters)

          if (!response.containsKey(ParameterKeys.Result) || response.getValue(ParameterKeys.Result).equals(ParameterValues.FALSE)) {
            module.log("VerifySignature: continueSignatureVerification failed")
          } else {
            module.log("VerifySignature: continueSignatureVerification success")
          }
        }
      )
      ChipDnaMobile.getInstance().clearAllSignatureCaptureListeners()
      ChipDnaMobile.getInstance().addSignatureCaptureListener(
        SignatureCaptureListener(
          module, promise, paymentDetails
        ) {
          module.log("VerifySignature: Signature verification callback")
        }
      )
      ChipDnaMobile.getInstance().clearAllTransactionFinishedListener()
      ChipDnaMobile.getInstance().addTransactionFinishedListener(
        TransactionFinishedListener(
          module, promise, paymentDetails
        ) {
          module.log("VerifySignature: Signature verified and capture")
          ClosePayment(
            module, promise, it
          ).execute(it.getId(), it.getAmount().toString(), null)
        }
      )
      requestParameters.add(ParameterKeys.SignatureData, params[0])
      return ChipDnaMobile.getInstance().continueSignatureCapture(requestParameters)
    }

    override fun onPostExecute(response: Parameters) {
      if (
        response.containsKey(ParameterKeys.Result)
        && response.getValue(ParameterKeys.Result).equals(ParameterValues.TRUE)
      ) {
        module.log("Capture success")
        callback?.invoke()
      } else {
        promise.reject("Signature", "Signature failed due to ${response.getValue(ParameterKeys.Errors)}")
      }
    }
  }

  class CaptureSignature(
    private val module: ChipDNAMobileModule,
    private val promise: Promise,
    private val paymentDetails: PaymentDetails,
    private val callback: (() -> Unit)? = null
  ) : AsyncTask<String?, Void?, Parameters>() {

    override fun doInBackground(vararg params: String?): Parameters? {
      val requestParameters = Parameters()
      module.log("Do signature capture")
      ChipDnaMobile.getInstance().clearAllSignatureCaptureListeners()
      ChipDnaMobile.getInstance().addSignatureCaptureListener(
        SignatureCaptureListener(
          module, promise, paymentDetails
        ) {
          ClosePayment(
            module, promise, paymentDetails
          ).execute(paymentDetails.getId(), paymentDetails.getAmount().toString(), null)
        }
      )
      ChipDnaMobile.getInstance().clearAllTransactionFinishedListener()
      ChipDnaMobile.getInstance().addTransactionFinishedListener(
        TransactionFinishedListener(
          module, promise, paymentDetails
        ) {
          ChipDnaMobile.getInstance().clearAllTransactionFinishedListener()
          ChipDnaMobile.getInstance().addTransactionFinishedListener(
            TransactionFinishedListener(
              module, promise, it
            )
          )
          ClosePayment(
            module, promise, it
          ).execute(it.getId(), it.getAmount().toString(), null)
        }
      )
      requestParameters.add(ParameterKeys.SignatureData, params[0])
      return ChipDnaMobile.getInstance().continueSignatureCapture(requestParameters)
    }

    override fun onPostExecute(response: Parameters) {
      if (
        response.containsKey(ParameterKeys.Result)
        && response.getValue(ParameterKeys.Result).equals(ParameterValues.TRUE)
      ) {
        module.log("Capture success")
        callback?.invoke()
      } else {
        promise.reject("Signature", "Signature failed due to ${response.getValue(ParameterKeys.Errors)}")
      }
    }
  }

  class ClosePayment(
    private val module: ChipDNAMobileModule,
    private val promise: Promise,
    private val paymentDetails: PaymentDetails
  ) : AsyncTask<String?, Void?, Parameters>() {

    override fun doInBackground(vararg params: String?): Parameters? {
      val requestParameters = Parameters()
      requestParameters.add(ParameterKeys.UserReference, params[0])
      requestParameters.add(ParameterKeys.Amount, params[1])
      requestParameters.add(ParameterKeys.TipAmount, params[2])
      requestParameters.add(ParameterKeys.CloseTransaction, ParameterValues.TRUE)
      module.statusUpdate("ConfirmTransaction")
      return ChipDnaMobile.getInstance().confirmTransaction(requestParameters)
    }

    override fun onPostExecute(response: Parameters) {
      if (
        response.containsKey(ParameterKeys.TransactionResult)
        && response.getValue(ParameterKeys.TransactionResult).equals(ParameterValues.Approved)
      ) {
        promise.resolve(paymentDetails.toResponse())
      } else {
        promise.reject("Payment", "Payment failed ${response.getValue(ParameterKeys.Result)} ${response.getValue(ParameterKeys.Errors)}")
      }
    }
  }

  @ReactMethod
  fun login(password: String, apiKey: String, environment: String, appId: String, promise: Promise) {
    if (ChipDnaMobile.isInitialized()) {
      val requestParameters = Parameters()
      requestParameters.add(ParameterKeys.ApiKey, apiKey)
      if (environment == "LIVE") requestParameters.add(ParameterKeys.Environment, ParameterValues.LiveEnvironment)
      else requestParameters.add(ParameterKeys.Environment, ParameterValues.TestEnvironment)
      requestParameters.add(ParameterKeys.ApplicationIdentifier, appId.toUpperCase())
      ChipDnaMobile.getInstance().setProperties(requestParameters)
      promise.resolve(true)
    } else {
      Login(this, promise) {
        val requestParameters = Parameters()
        requestParameters.add(ParameterKeys.ApiKey, apiKey)
        if (environment == "LIVE") requestParameters.add(ParameterKeys.Environment, ParameterValues.LiveEnvironment)
        else requestParameters.add(ParameterKeys.Environment, ParameterValues.TestEnvironment)
        requestParameters.add(ParameterKeys.ApplicationIdentifier, appId.toUpperCase())
        requestParameters.add(ParameterKeys.DigitalSignatureSupported, ParameterValues.TRUE)
        ChipDnaMobile.getInstance().setProperties(requestParameters)
        ChipDnaMobile.getInstance().addTransactionUpdateListener(
          TransactionUpdateListener(this)
        )
        promise.resolve(true)
      }.execute(password)
    }
  }

  @ReactMethod
  fun loggedIn(promise: Promise) {
    if (ChipDnaMobile.isInitialized()) {
      val statusParameters = ChipDnaMobile.getInstance().getStatus(null)
      log("API Key: ${statusParameters.getValue(ParameterKeys.ApiKey)}")
      promise.resolve(statusParameters.getValue(ParameterKeys.ApiKey) != null)
    } else {
      log("Not initialized")
      promise.resolve(false)
    }
  }

  @ReactMethod
  fun getDevices(promise: Promise) {
    checkBluetoothEnabled(promise)
    val parameters = Parameters()
    parameters.add(ParameterKeys.SearchConnectionTypeBluetooth, ParameterValues.TRUE)
    parameters.add(ParameterKeys.SearchConnectionTypeUsb, ParameterValues.TRUE)

    ChipDnaMobile.getInstance().clearAllAvailablePinPadsListeners()
    ChipDnaMobile.getInstance().addAvailablePinPadsListener(AvailablePinPadsListener(this, promise))
    ChipDnaMobile.getInstance().getAvailablePinPads(parameters)
  }

  @ReactMethod
  fun connectDevice(device: ReadableMap, promise: Promise) {
    log("Connecting...")

    if (device.hasKey("name") && device.hasKey("connectionType")) {
      Connect(this, promise) {
        ChipDnaMobile.getInstance().clearAllConnectAndConfigureFinishedListeners()
        ChipDnaMobile.getInstance().addConnectAndConfigureFinishedListener(
          ConnectAndConfigureFinishedListener(
            this, promise
          )
        )
        val response = ChipDnaMobile.getInstance().connectAndConfigure(ChipDnaMobile.getInstance().getStatus(null))
        if (response.containsKey(ParameterKeys.Result) && response.getValue(ParameterKeys.Result) == ParameterValues.FALSE) {
          log("Error: " + response.getValue(ParameterKeys.Errors))
          promise.reject("Device", response.getValue(ParameterKeys.Errors))
        }
      }.execute(device.getString("name"), device.getString("connectionType"))
    } else promise.reject("Device", "Both 'name' and 'connectionType' are required")
  }

  @ReactMethod
  fun reconnectDevice(promise: Promise) {
    val statusParameters = ChipDnaMobile.getInstance().getStatus(null)

    if (
      statusParameters.getValue(ParameterKeys.PinPadName) != null
      && statusParameters.getValue(ParameterKeys.PinPadName).isNotEmpty()
      && statusParameters.getValue(ParameterKeys.PinPadConnectionType) != null
      && statusParameters.getValue(ParameterKeys.PinPadConnectionType).isNotEmpty()
    ) {
      Connect(this, promise) {
        log("Reconnected to pin pad")
        ChipDnaMobile.getInstance().clearAllConnectAndConfigureFinishedListeners()
        ChipDnaMobile.getInstance().addConnectAndConfigureFinishedListener(
          ConnectAndConfigureFinishedListener(
            this, promise
          )
        )
        val response = ChipDnaMobile.getInstance().connectAndConfigure(ChipDnaMobile.getInstance().getStatus(null))
        if (response.containsKey(ParameterKeys.Result) && response.getValue(ParameterKeys.Result) == ParameterValues.FALSE) {
          log("Error: " + response.getValue(ParameterKeys.Errors))
          promise.reject("Device", response.getValue(ParameterKeys.Errors))
        }
      }.execute(
        statusParameters.getValue(ParameterKeys.PinPadName),
        statusParameters.getValue(ParameterKeys.PinPadConnectionType)
      )
    }
  }

  @ReactMethod
  fun isDeviceConnected(promise: Promise) {
    val statusParameters = ChipDnaMobile.getInstance().getStatus(null)

    statusParameters.toList().forEach {
      log("${it.key}=${it.value}")
    }

    if (statusParameters.containsKey(ParameterKeys.DeviceStatus)) {
      val deviceStatus = ChipDnaMobileSerializer.deserializeDeviceStatus(statusParameters.getValue(ParameterKeys.DeviceStatus))

      promise.resolve(deviceStatus.status === DeviceStatus.DeviceStatusEnum.DeviceStatusConnected)
    }
  }

  @ReactMethod
  fun makePayment(amount: Int, currency: String, promise: Promise) {
    val requestParameters = Parameters()
    val paymentDetails = PaymentDetails(
      UUID.randomUUID().toString(),
      amount,
      PaymentStatus.UNKNOWN
    )

    requestParameters.add(ParameterKeys.Amount, amount)
    requestParameters.add(ParameterKeys.AmountType, ParameterValues.AmountTypeActual)
    requestParameters.add(ParameterKeys.Currency, currency)

    requestParameters.add(ParameterKeys.UserReference, paymentDetails.getId())
    requestParameters.add(ParameterKeys.TransactionType, ParameterValues.Sale)
    requestParameters.add(ParameterKeys.PaymentMethod, ParameterValues.Card)

    // Use an instance of ChipDnaMobile to begin startTransaction.
    ChipDnaMobile.getInstance().clearAllTransactionFinishedListener()
    ChipDnaMobile.getInstance().addTransactionFinishedListener(
      TransactionFinishedListener(
        this, promise, paymentDetails
      ) {
        ChipDnaMobile.getInstance().clearAllTransactionFinishedListener()
        ChipDnaMobile.getInstance().addTransactionFinishedListener(
          TransactionFinishedListener(
            this, promise, it
          )
        )
        ClosePayment(
          this, promise, it
        ).execute(paymentDetails.getId(), paymentDetails.getAmount().toString(), null)
      }
    )
    ChipDnaMobile.getInstance().clearAllApplicationSelectionListener()
    ChipDnaMobile.getInstance().addApplicationSelectionListener(
      ApplicationSelectionListener(
        this, promise, paymentDetails
      )
    )
    ChipDnaMobile.getInstance().clearAllUserNotificationListener()
    ChipDnaMobile.getInstance().addUserNotificationListener(
      UserNotificationListener(
        this, promise, paymentDetails
      )
    )
    ChipDnaMobile.getInstance().clearAllSignatureVerificationListener()
    ChipDnaMobile.getInstance().addSignatureVerificationListener(
      SignatureVerificationListener(
        this, promise, paymentDetails
      ) {
        log("makePayment: Signature verification callback")
        val approveSignatureParameters = Parameters()
        approveSignatureParameters.add(ParameterKeys.Result, ParameterValues.TRUE)

        val response = ChipDnaMobile.getInstance().continueSignatureVerification(approveSignatureParameters)

        if (!response.containsKey(ParameterKeys.Result) || response.getValue(ParameterKeys.Result).equals(ParameterValues.FALSE)) {
          log("makePayment: continueSignatureVerification failed")
        } else {
          log("makePayment: continueSignatureVerification success")
        }
      }
    )
    ChipDnaMobile.getInstance().clearAllSignatureCaptureListeners()
    ChipDnaMobile.getInstance().addSignatureCaptureListener(
      SignatureCaptureListener(
        this, promise, paymentDetails
      )
    )
    log("Make payment")
    val response = ChipDnaMobile.getInstance().startTransaction(requestParameters)

    if (response.containsKey(ParameterKeys.Result) && response.getValue(ParameterKeys.Result) == ParameterValues.FALSE) {
      log("Error: " + response.getValue(ParameterKeys.Errors))
      promise.reject("Payment", response.getValue(ParameterKeys.Errors))
    }
  }

  @ReactMethod
  fun cancelVerification(promise: Promise) {
    log("Cancel payment")
    val declineParameters = Parameters()
    declineParameters.add(ParameterKeys.Result, ParameterValues.FALSE)
    val response = ChipDnaMobile.getInstance().continueSignatureVerification(declineParameters)

    if (!response.containsKey(ParameterKeys.Result) || response.getValue(ParameterKeys.Result) == ParameterValues.FALSE) {
      promise.resolve(false)
    } else {
      promise.resolve(true)
    }
  }

  @ReactMethod
  fun verifyPayment(payment: ReadableMap, verification: String, promise: Promise) {
    log("Verify payment")
    val paymentDetails = PaymentDetails(payment)

    val parameters = Parameters()

    if (
      paymentDetails.getStatus() === PaymentStatus.PIN_REQUIRED
      || (
        verification.isEmpty()
        && paymentDetails.getStatus() === PaymentStatus.SIGNATURE_REQUIRED
      )
    ) {
      parameters.add(ParameterKeys.Result, ParameterValues.TRUE)
      parameters.add(ParameterKeys.OperatorPin, verification)
      ChipDnaMobile.getInstance().clearAllSignatureVerificationListener()
      ChipDnaMobile.getInstance().addSignatureVerificationListener(
        SignatureVerificationListener(
          this, promise, paymentDetails
        ) {
          log("verifyPayment: Signature verification callback")
          val approveSignatureParameters = Parameters()
          approveSignatureParameters.add(ParameterKeys.Result, ParameterValues.TRUE)

          val response = ChipDnaMobile.getInstance().continueSignatureVerification(approveSignatureParameters)

          if (!response.containsKey(ParameterKeys.Result) || response.getValue(ParameterKeys.Result).equals(ParameterValues.FALSE)) {
            log("verifyPayment: continueSignatureVerification failed")
          } else {
            log("verifyPayment: continueSignatureVerification success")
          }
        }
      )
      ChipDnaMobile.getInstance().clearAllTransactionFinishedListener()
      ChipDnaMobile.getInstance().addTransactionFinishedListener(
        TransactionFinishedListener(
          this, promise, paymentDetails
        ) {
          ChipDnaMobile.getInstance().clearAllTransactionFinishedListener()
          ChipDnaMobile.getInstance().addTransactionFinishedListener(
            TransactionFinishedListener(
              this, promise, it
            )
          )
          ClosePayment(
            this, promise, it
          ).execute(paymentDetails.getId(), paymentDetails.getAmount().toString(), null)
        }
      )
      ChipDnaMobile.getInstance().continueSignatureVerification(parameters)
    } else if (paymentDetails.getStatus() === PaymentStatus.SIGNATURE_REQUIRED) {
      VerifySignature(this, promise, paymentDetails).execute(verification)
    } else promise.reject("Verification", "Invalid verification data sent")
  }

  @ReactMethod
  fun updateTMS(promise: Promise) {
    log("Update TMS")
    val tmsParams = Parameters()
    tmsParams.add(ParameterKeys.ForceTmsUpdate, ParameterValues.TRUE)
    tmsParams.add(ParameterKeys.FullTmsUpdate, ParameterValues.FALSE)
    val response = ChipDnaMobile.getInstance().requestTmsUpdate(tmsParams)

    if (!response.containsKey(ParameterKeys.Result) || response.getValue(ParameterKeys.Result) == ParameterValues.FALSE) {
      log("TMS Update failed")
      promise.resolve(false)
    } else {
      log("TMS Updated")
      promise.resolve(true)
    }
  }

  @ReactMethod
  fun addListener(eventName: String) {
    // Required so that ReactNative doesn't warn us about this function not existing.
  }

  @ReactMethod
  fun removeListeners(count: Int) {
    // Required so that ReactNative doesn't warn us about this function not existing.
  }

  private fun checkBluetoothEnabled(promise: Promise) {
    val blueToothAdapter = BluetoothAdapter.getDefaultAdapter()

    if (blueToothAdapter === null) promise.reject("Bluetooth", "There is no bluetooth on this device")
    else {
      if (!blueToothAdapter.isEnabled) {
        val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        val activity = reactApplicationContext.currentActivity
        if(activity != null) {
          ActivityCompat.startActivityForResult(activity, enableBluetoothIntent, 1, null)
        }
        log("Bluetooth Must Be Enabled")
      }
    }
  }
}
