package com.reactnativechipdnamobile

import android.content.Context
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.*

class ConnectAndConfigureFinishedListener(
  private val module: ChipDNAMobileModule,
  private val promise: Promise,
  private val callback: (() -> Unit)? = null
): IConnectAndConfigureFinishedListener {
  override fun onConnectAndConfigureFinished(parameters: Parameters) {
    promise.resolve(parameters.getValue(ParameterKeys.Result).equals(ParameterValues.TRUE))
  }
}
