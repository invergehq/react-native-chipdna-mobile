package com.reactnativechipdnamobile

import android.content.Context
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.*

class AvailablePinPadsListener(
  private val module: ChipDNAMobileModule,
  private val promise: Promise,
  private val callback: (() -> Unit)? = null
) : IAvailablePinPadsListener {
  class AvailablePinPads(
    private val module: ChipDNAMobileModule,
    private val promise: Promise,
    private val callback: (() -> Unit)? = null
  ) : AsyncTask<String?, Void?, NativeArray>() {
    override fun doInBackground(vararg params: String?): NativeArray? {
      val availablePinPadsList: WritableNativeArray = WritableNativeArray()
      try {
        val availablePinPadsHashMap = ChipDnaMobileSerializer.deserializeAvailablePinPads(params[0])
        for (connectionType in availablePinPadsHashMap.keys) {
          for (pinpad in availablePinPadsHashMap[connectionType]!!) {
            module.log("$pinpad $connectionType")
            val selectablePinPad: WritableNativeMap = WritableNativeMap()
            selectablePinPad.putString("name", pinpad)
            selectablePinPad.putString("connectionType", connectionType)
            availablePinPadsList.pushMap(selectablePinPad)
          }
        }
      } catch (e: XmlPullParserException) {
        e.printStackTrace()
      } catch (e: IOException) {
        e.printStackTrace()
      } catch (e: java.lang.NullPointerException) {
        e.printStackTrace()
      }

      return availablePinPadsList
    }

    override fun onPostExecute(availablePinPadsList: NativeArray?) {
      promise.resolve(availablePinPadsList)
    }
  }

  override fun onAvailablePinPads(parameters: Parameters) {
    val availablePinPadsXml = parameters.getValue(ParameterKeys.AvailablePinPads)
    AvailablePinPads(module, promise).execute(availablePinPadsXml)
  }
}
