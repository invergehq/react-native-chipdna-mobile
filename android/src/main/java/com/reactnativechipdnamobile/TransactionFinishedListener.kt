package com.reactnativechipdnamobile

import android.content.Context
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.*

class TransactionFinishedListener(
  private val module: ChipDNAMobileModule,
  private val promise: Promise,
  private val paymentDetails: PaymentDetails,
  private val callback: ((PaymentDetails) -> Unit)? = null
): ITransactionFinishedListener {
  override fun onTransactionFinishedListener(parameters: Parameters) {
    module.log("Transaction finished ${parameters.getValue(ParameterKeys.TransactionResult)}")
    if (
      callback === null
      || parameters.getValue(ParameterKeys.TransactionResult).equals(ParameterValues.Declined)
    ) {
      promise.resolve(
        PaymentDetails(parameters).toResponse()
      )
    } else callback.invoke(PaymentDetails(parameters))
  }
}
