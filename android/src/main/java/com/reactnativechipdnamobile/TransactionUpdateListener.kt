package com.reactnativechipdnamobile

import android.content.Context
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import org.xmlpull.v1.XmlPullParserException
import java.io.IOException
import java.util.*

class TransactionUpdateListener(
  private val module: ChipDNAMobileModule
): ITransactionUpdateListener {
  override fun onTransactionUpdateListener(parameters: Parameters) {
    module.statusUpdate(parameters.getValue(ParameterKeys.TransactionUpdate))
    module.log("Transaction update: ${parameters.getValue(ParameterKeys.TransactionUpdate)}")
  }
}
