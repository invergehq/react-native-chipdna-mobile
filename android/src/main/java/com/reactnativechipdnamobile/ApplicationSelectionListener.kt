package com.reactnativechipdnamobile

import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import java.util.*

class ApplicationSelectionListener(
  private val module: ChipDNAMobileModule,
  private val promise: Promise,
  private val paymentDetails: PaymentDetails,
  private val callback: (() -> Unit)? = null
): IApplicationSelectionListener {
  override fun onApplicationSelection(parameters: Parameters) {
    if (parameters.getValue(ParameterKeys.ResponseRequired).equals(ParameterValues.TRUE)) {
      module.log("Application selection required");
      module.log(parameters.getValue(ParameterKeys.AvailableApplications))
    } else {
      module.log("Application selection not required");
      callback?.invoke()
    }
  }
}
