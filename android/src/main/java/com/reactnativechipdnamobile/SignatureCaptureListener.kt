package com.reactnativechipdnamobile

import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import java.util.*

class SignatureCaptureListener(
  private val module: ChipDNAMobileModule,
  private val promise: Promise,
  private val paymentDetails: PaymentDetails,
  private val callback: (() -> Unit)? = null
): ISignatureCaptureListener {
  override fun onSignatureCapture(parameters: Parameters) {
    module.log("Signature captured");

    if (parameters.getValue(ParameterKeys.ResponseRequired).equals(ParameterValues.TRUE)) {
      module.log("Signature Check Required");
      if (parameters.getValue(ParameterKeys.OperatorPinRequired) == ParameterValues.TRUE) {
        promise.resolve(
          PaymentDetails(
            paymentDetails,
            PaymentStatus.PIN_REQUIRED
          ).toResponse()
        )
      } else {
        promise.resolve(
          PaymentDetails(
            paymentDetails,
            PaymentStatus.SIGNATURE_REQUIRED
          ).toResponse()
        )
      }
    } else {
      callback?.invoke()
    }
  }
}
