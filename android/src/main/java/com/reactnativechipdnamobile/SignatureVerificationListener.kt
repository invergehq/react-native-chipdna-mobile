package com.reactnativechipdnamobile

import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import java.util.*

class SignatureVerificationListener(
  private val module: ChipDNAMobileModule,
  private val promise: Promise,
  private val paymentDetails: PaymentDetails,
  private val callback: (() -> Unit)? = null
): ISignatureVerificationListener {
  override fun onSignatureVerification(parameters: Parameters) {
    if (parameters.getValue(ParameterKeys.ResponseRequired).equals(ParameterValues.TRUE)) {
      module.log("Signature Check Required");
      if (parameters.getValue(ParameterKeys.OperatorPinRequired) == ParameterValues.TRUE) {
        module.log("Pin Required")
        promise.resolve(
          PaymentDetails(
            paymentDetails,
            PaymentStatus.PIN_REQUIRED
          ).toResponse()
        )
      } else if (false && parameters.getValue(ParameterKeys.DigitalSignatureSupported).equals(ParameterValues.TRUE)) {
        module.log("Digital Signature Required")
        promise.resolve(
          PaymentDetails(
            paymentDetails,
            PaymentStatus.SIGNATURE_REQUIRED
          ).toResponse()
        )
      } else {
        module.log("Physical Signature Required")
        callback?.invoke()
      }
    } else {
      module.log("Signature Check On Keypad");
      callback?.invoke()
    }
  }
}
