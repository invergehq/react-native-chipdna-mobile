package com.reactnativechipdnamobile

import com.creditcall.chipdnamobile.*
import com.facebook.react.bridge.*
import java.util.*

class UserNotificationListener(
  private val module: ChipDNAMobileModule,
  private val promise: Promise,
  private val paymentDetails: PaymentDetails,
  private val callback: (() -> Unit)? = null
): IUserNotificationListener {
  override fun onUserNotification(parameters: Parameters) {
    if (parameters.getValue(ParameterKeys.ResponseRequired).equals(ParameterValues.TRUE)) {
      module.log("User notification response required");
    } else {
      module.log("User notification response not required");
      callback?.invoke()
    }
  }
}
