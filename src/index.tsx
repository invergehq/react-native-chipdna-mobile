import { NativeModules } from 'react-native';

export type DeviceType = {
  name: string;
  connectionType: string;
};

export type PaymentType = {
  id: string;
  status: ChipDNAMobilePaymentStatus;
  transactionId: string;
  amount: number;
  cardId: string;
  maskedPan: string;
  receiptData: ReceiptDataType;
};

type ReceiptDataType = {
  'cem.receipt.app.preferred.name': ReceiptDataLineType;
  'cem.receipt.applicationid': ReceiptDataLineType;
  'cem.receipt.authcode': ReceiptDataLineType;
  'cem.receipt.cereference': ReceiptDataLineType;
  'cem.receipt.cvm': ReceiptDataLineType;
  'cem.receipt.datetime': ReceiptDataLineType;
  'cem.receipt.footer': ReceiptDataLineType;
  'cem.receipt.header': ReceiptDataLineType;
  'cem.receipt.maskedcardnumber': ReceiptDataLineType;
  'cem.receipt.merchantnumber': ReceiptDataLineType;
  'cem.receipt.nameandaddress': ReceiptDataLineType;
  'cem.receipt.pansequencenumber': ReceiptDataLineType;
  'cem.receipt.result': ReceiptDataLineType;
  'cem.receipt.retention': ReceiptDataLineType;
  'cem.receipt.terminalid': ReceiptDataLineType;
  'cem.receipt.transactioncurrency': ReceiptDataLineType;
  'cem.receipt.transactionsource': ReceiptDataLineType;
  'cem.receipt.transactiontype': ReceiptDataLineType;
  'cem.receipt.transcationtotal': ReceiptDataLineType;
};

type ReceiptDataLineType = {
  label: string;
  value: string;
};

export type PaymentResponseType = {
  accountNumber: string;
  accountType: string;
  authCode: string;
  authorizedAmount: string;
  avsResultCode?: {
    value: string;
    description: string;
  };
  cardCodeResponse?: {
    value: string;
    description: string;
  };
  cavvResultCode?: {
    value: string;
    description: string;
  };
  emvTLVResponse: string;
  emvTlvMap?: {
    [key: string]: string;
  };
  entryMode: string;
  isAuthorizeNet: boolean;
  isIssuerResponse: boolean;
  isShowSignature: boolean;
  isTestRequest: boolean;
  merchantDefinedMap?: {
    [key: string]: string;
  };
  prepaidCard?: {
    approvedAmount: number;
    balanceOnCard: number;
    requestedAmount: number;
  };
  refTransId: string;
  responseCode: {
    code: string;
    description: string;
  };
  responseText: string;
  signatureBase64: string;
  splitTenderId: string;
  splitTenderPayments?: {
    accountNumber: string;
    accountType: string;
    approvedAmount: number;
    authCode: string;
    balanceOnCard: number;
    requestedAmount: number;
    responseCode: {
      code: string;
      description: string;
    };
    responseToCustomer: string;
    transId: string;
  };
  tipAmount: string;
  transactionResponseErrors?: {
    notes: string;
    reasonText: string;
    responseCode: {
      code: string;
      description: string;
    };
    responseReasonCode: number;
  }[];
  transactionResponseMessages?: {
    notes: string;
    reasonText: string;
    responseCode: {
      code: string;
      description: string;
    };
    responseReasonCode: number;
  }[];
  transHash: string;
  transId: string;
};

export type MerchantDetailsType = {
  companyName: string;
  address: string;
  city: string;
  state: string;
  zip: string;
};

type ChipDNAMobileType = {
  login(
    password: string,
    apiKey: string,
    environment: ChipDNAMobileEnvironment,
    appId: string
  ): Promise<{
    name: string;
    device: string;
  }>;
  loggedIn(): Promise<boolean>;
  getMerchantDetails(): Promise<MerchantDetailsType>;
  getDevices(): Promise<DeviceType[]>;
  connectDevice(device: DeviceType): Promise<boolean>;
  reconnectDevice(): Promise<boolean>;
  isDeviceConnected(): Promise<boolean>;
  makePayment(amount: number, currency: string): Promise<PaymentType>;
  cancelVerification(): Promise<boolean>;
  verifyPayment(
    payment: PaymentType,
    verification: string
  ): Promise<PaymentType>;
  hasCardData(): Promise<boolean>;
  updateTMS(): Promise<boolean>;
};

export enum ChipDNAMobileEnvironment {
  LIVE = 'LIVE',
  TEST = 'STAGING',
}

export enum ChipDNAMobilePaymentStatus {
  PIN_REQUIRED = 'PIN_REQUIRED',
  SIGNATURE_REQUIRED = 'SIGNATURE_REQUIRED',
  APPROVED = 'APPROVED',
  DECLINED = 'DECLINED',
}

const { ChipDNAMobile } = NativeModules;

export default ChipDNAMobile as ChipDNAMobileType;
