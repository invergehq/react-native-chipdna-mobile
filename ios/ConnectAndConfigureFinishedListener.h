#import <React/RCTBridgeModule.h>
#import "ChipDnaMobile/ChipDnaMobile.h"

@interface ConnectAndConfigureFinishedListener : NSObject 

- (id) initWithResolver:(RCTPromiseResolveBlock)resolve withRejecter:(RCTPromiseRejectBlock)reject;

- (void) connectAndConfigureFinishedListener:(CCParameters *)parameters;

@end