#import "ChipDnaMobile/ChipDnaMobile.h"

typedef NSString* PaymentStatus;
extern PaymentStatus const PaymentStatusUnknown;
extern PaymentStatus const PaymentStatusApproved;
extern PaymentStatus const PaymentStatusDeclined;
extern PaymentStatus const PaymentStatusPinRequired;
extern PaymentStatus const PaymentStatusSignatureRequired;

@interface PaymentDetails : NSObject 

- (id) initWithAmount:(int)amount 
       withPaymentStatus:(NSString *)status;

- (id) initWithPaymentDetails:(PaymentDetails *)paymentDetails 
       withPaymentStatus:(PaymentStatus)status;

- (id) initWithParameters:(CCParameters *)parameters;

- (id) initWithDictionary:(NSDictionary *)details;

- (NSString *) getId;

- (int) getAmount;

- (PaymentStatus) getStatus;

- (NSDictionary *) toResponse;

@end