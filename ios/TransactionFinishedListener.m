#import <React/RCTLog.h>
#import "TransactionFinishedListener.h"

@implementation TransactionFinishedListener  {
  RCTPromiseResolveBlock resolve;
  RCTPromiseRejectBlock reject;
  void(^callback)(PaymentDetails *);
  ChipDnaModule *module;
}

- (id) initWithResolver:(RCTPromiseResolveBlock)resolver 
       withRejecter:(RCTPromiseRejectBlock)rejecter 
       withCallback:(void(^)(PaymentDetails *))callbackFunction
       withModule:(ChipDnaModule *)m
{
  if (self = [super init]) {
    resolve = resolver;
    reject = rejecter;
    callback = callbackFunction;
    module = m;
  }

  return self;
}

- (void) transactionFinishedListener:(CCParameters *)parameters {
    NSString *result = [parameters valueForKey:CCParamTransactionResult];
    [module sendMessage:[NSString stringWithFormat:@"Transaction finished %@", result]];

    PaymentDetails *paymentDetails = [[PaymentDetails alloc] initWithParameters:parameters];

    if (callback == nil || [result isEqualToString:CCValueDeclined]) {
      resolve([paymentDetails toResponse]);
    } else {
      callback(paymentDetails);
    }
}

@end
