#import "ChipDnaMobile/ChipDnaMobile.h"
#import "ChipDnaModule.h"

@interface ApplicationSelectionListener : NSObject 

- (id) initWithModule:(ChipDnaModule *)module;

- (void) applicationSelectionListener:(CCParameters *)parameters;

@end