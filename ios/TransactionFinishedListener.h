#import <React/RCTBridgeModule.h>
#import "ChipDnaMobile/ChipDnaMobile.h"
#import "PaymentDetails.h"
#import "ChipDnaModule.h"


@interface TransactionFinishedListener : NSObject 

- (id) initWithResolver:(RCTPromiseResolveBlock)resolve 
       withRejecter:(RCTPromiseRejectBlock)reject
       withCallback:(void (^)(PaymentDetails *))callback
       withModule:(ChipDnaModule*)module;

- (void) transactionFinishedListener:(CCParameters *)parameters;

@end
