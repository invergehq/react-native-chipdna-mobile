#import <React/RCTBridgeModule.h>
#import "ChipDnaMobile/ChipDnaMobile.h"
#import "PaymentDetails.h"
#import "ChipDnaModule.h"


@interface SignatureVerificationListener : NSObject 

- (id) initWithResolver:(RCTPromiseResolveBlock)resolver 
       withRejecter:(RCTPromiseRejectBlock)rejecter 
       withPaymentDetails:(PaymentDetails *)paymentDetails
       withCallback:(void(^)())callbackFunction
       withModule:(ChipDnaModule *)module;

- (void) signatureVerificationListener:(CCParameters *)parameters;

@end
