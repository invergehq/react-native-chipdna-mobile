#import <React/RCTLog.h>
#import "ConnectAndConfigureFinishedListener.h"

@implementation ConnectAndConfigureFinishedListener  {
  RCTPromiseResolveBlock resolve;
  RCTPromiseRejectBlock reject;
}

- (id) initWithResolver:(RCTPromiseResolveBlock)resolver withRejecter:(RCTPromiseRejectBlock)rejecter {
  if(self = [super init]) {
    resolve = resolver;
    reject = rejecter;
  }

  return self;
}

- (void) connectAndConfigureFinishedListener:(CCParameters *)parameters {
    NSString *result = [parameters valueForKey:CCParamResult];
    resolve([NSNumber numberWithBool:[result isEqualToString:CCValueTrue]]);
}

@end
