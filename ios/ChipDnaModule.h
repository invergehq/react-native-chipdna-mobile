#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import "ChipDnaMobile/ChipDnaMobile.h"

@interface ChipDnaModule : RCTEventEmitter <RCTBridgeModule>

- (void) statusUpdate:(NSString *)message;
- (void) sendMessage:(NSString *)message;
- (void) transactionUpdateListener:(CCParameters *)parameters;

+ (void) addAvailablePinPadsTarget:(id)target action:(SEL)action;
// + (void) addTransactionUpdateTarget:(id)target action:(SEL)action;
+ (void) addConnectAndConfigureFinishedTarget:(id)target action:(SEL)action;
+ (void) addTransactionFinishedTarget:(id)target action:(SEL)action;
+ (void) addCardApplicationSelectionTarget:(id)target action:(SEL)action;
+ (void) addUserNotificationTarget:(id)target action:(SEL)action;
+ (void) addSignatureVerificationTarget:(id)target action:(SEL)action;
+ (void) addSignatureCaptureTarget:(id)target action:(SEL)action;

@end