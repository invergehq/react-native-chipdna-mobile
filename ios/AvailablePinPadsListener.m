#import <React/RCTLog.h>
#import "AvailablePinPadsListener.h"
#import "ChipDnaMobile/ChipDnaMobileSerializer.h"

@implementation AvailablePinPadsListener  {
  RCTPromiseResolveBlock resolve;
  RCTPromiseRejectBlock reject;
  ChipDnaModule *module;
}

- (id) initWithResolver:(RCTPromiseResolveBlock)resolver withRejecter:(RCTPromiseRejectBlock)rejecter withModule:(ChipDnaModule *)m {
  if(self = [super init]) {
    resolve = resolver;
    reject = rejecter;
    module = m;
  }

  return self;
}

- (void) availablePinPadsListener:(CCParameters *)parameters {
  NSString *xml = [parameters valueForKey:CCParamAvailablePinPads];
  NSDictionary *availablePinPads = [ChipDnaMobileSerializer deserializeAvailablePinPadsString:xml];

  NSMutableArray *availablePinPadsList = [[NSMutableArray alloc] init];

  for (NSString *connectionType in availablePinPads) {
    for (NSArray *pinpad in availablePinPads[connectionType]) {
      [module sendMessage:[NSString stringWithFormat:@"%@ %@", pinpad, connectionType]];
      [availablePinPadsList addObject:@{
        @"name": pinpad,
        @"connectionType": connectionType,
      }];
    }
  }

  resolve(availablePinPadsList);
}

@end