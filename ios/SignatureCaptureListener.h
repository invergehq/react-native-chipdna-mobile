#import <React/RCTBridgeModule.h>
#import "ChipDnaMobile/ChipDnaMobile.h"
#import "PaymentDetails.h"
#import "ChipDnaModule.h"


@interface SignatureCaptureListener : NSObject 

- (id) initWithResolver:(RCTPromiseResolveBlock)resolve 
       withRejecter:(RCTPromiseRejectBlock)reject
       withPaymentDetails:(PaymentDetails *)paymentDetails
       withModule:(ChipDnaModule *)module;

- (void) signatureCaptureListener:(CCParameters *)parameters;

@end
