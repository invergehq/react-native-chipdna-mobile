#import <React/RCTBridgeModule.h>
#import "ChipDnaMobile/ChipDnaMobile.h"
#import "ChipDnaModule.h"

@interface AvailablePinPadsListener : NSObject 

- (id) initWithResolver:(RCTPromiseResolveBlock)resolve withRejecter:(RCTPromiseRejectBlock)reject withModule:(ChipDnaModule *)module;

- (void) availablePinPadsListener:(CCParameters *)parameters;

@end
