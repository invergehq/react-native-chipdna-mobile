#import "ChipDnaMobile/ChipDnaMobile.h"
#import "ChipDnaModule.h"

@interface UserNotificationListener : NSObject 

- (id) initWithModule:(ChipDnaModule*)module;

- (void) userNotificationListener:(CCParameters *)parameters;

@end