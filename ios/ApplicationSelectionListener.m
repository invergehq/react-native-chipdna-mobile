#import <React/RCTLog.h>
#import "ApplicationSelectionListener.h"

@implementation ApplicationSelectionListener {
  ChipDnaModule *module;
}

- (id) initWithModule:(ChipDnaModule *)m {
  if (self = [super init]) {
    module = m;
  }

  return self;
}

- (void) applicationSelectionListener:(CCParameters *)parameters {
    NSString *responseRequired = [parameters valueForKey:CCParamResponseRequired];

    if ([responseRequired isEqualToString:CCValueTrue]) {
      [module sendMessage:@"Application selection required"];
      [module sendMessage:[parameters valueForKey:CCParamAvailableCardApplications]];
    } else {
      [module sendMessage:@"Application selection not required"];
    }
}

@end
