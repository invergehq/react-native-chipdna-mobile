#import <Foundation/Foundation.h>
#import "ChipDnaMobile/ChipDnaMobileSerializer.h"
#import <React/RCTLog.h>
#import "PaymentDetails.h"

PaymentStatus const PaymentStatusUnknown = @"UNKNOWN";
PaymentStatus const PaymentStatusApproved = @"APPROVED";
PaymentStatus const PaymentStatusDeclined = @"DECLINED";
PaymentStatus const PaymentStatusPinRequired = @"PIN_REQUIRED";
PaymentStatus const PaymentStatusSignatureRequired = @"SIGNATURE_REQUIRED";

@implementation PaymentDetails {
  NSString *identifier;
  NSString *status;
  NSString *transactionId;
  NSString *customerId;
  NSString *cardId;
  NSString *maskedPan;
  NSMutableDictionary *receiptData;
  int amount;
}

- (id) initWithAmount:(int)a 
       withPaymentStatus:(NSString *)s 
{
  if (self = [super init]) {
    identifier = [[NSUUID UUID] UUIDString];
    status = s;
    transactionId = nil;
    customerId = nil;
    cardId = nil;
    maskedPan = nil;
    receiptData = nil;
    amount = a;
  }

  return self;
}

- (id) initWithPaymentDetails:(PaymentDetails *)paymentDetails 
       withPaymentStatus:(PaymentStatus)s 
{
  if (self = [super init]) {
    identifier = [paymentDetails getId];
    status = s;
    transactionId = nil;
    customerId = nil;
    cardId = nil;
    maskedPan = nil;
    receiptData = nil;
    amount = [paymentDetails getAmount];
  }

  return self;
}

- (id) initWithParameters:(CCParameters *)parameters {
  if (self = [super init]) {
    identifier = [parameters valueForKey:CCParamUserReference];

    NSString *transactionResult = [parameters valueForKey:CCParamTransactionResult];
    if (!transactionResult || [transactionResult isEqualToString:CCValueDeclined]) {
      status = PaymentStatusDeclined;
    } else {
      status = PaymentStatusApproved;
    }

    transactionId = [parameters valueForKey:CCParamTransactionId];
    customerId = [parameters valueForKey:CCParamCustomerVaultId];
    cardId = [parameters valueForKey:CCParamCardReference];
    amount = [[parameters valueForKey:CCParamAmount] intValue];
    maskedPan = [parameters valueForKey:CCParamMaskedPan];

    NSString *receiptData = [parameters valueForKey:CCParamReceiptData];

    if (receiptData) {
      NSDictionary *receipt = [ChipDnaMobileSerializer deserializeReceiptData:receiptData];
      NSMutableDictionary *currentReceiptData = [[NSMutableDictionary alloc] init];

      for (id key in receipt) {
        ReceiptField* field = [receipt objectForKey:key];
        NSDictionary *value = @{
          @"label": field.labelString ? field.labelString : @"",
          @"value": field.value,
        };
        [currentReceiptData setObject:value forKey:key];
      }

      receiptData = currentReceiptData;
    }
  }

  return self;
}

- (id) initWithDictionary:(NSDictionary *)details {
  if (self = [super init]) {
    transactionId = nil;
    customerId = nil;
    cardId = nil;
    maskedPan = nil;
    receiptData = nil;

    NSString *idParam = [details objectForKey:@"id"];
    NSString *statusParam = [details objectForKey:@"status"];
    NSString *amountParam = [details objectForKey:@"amount"];

    if (idParam) {
      identifier = idParam;
    } else {
      identifier = [[NSUUID UUID] UUIDString];
    }

    NSArray *allPaymentStatus = @[
      PaymentStatusUnknown, 
      PaymentStatusApproved, 
      PaymentStatusDeclined, 
      PaymentStatusPinRequired, 
      PaymentStatusSignatureRequired,
    ];

    if ([allPaymentStatus containsObject:statusParam]) {
      status = statusParam;
    } else {
      status = PaymentStatusUnknown;
    }

    if (amountParam) {
      amount = [amountParam intValue];
    } else {
      amount = 0;
    }
  }

  return self;
}

- (NSString *) getId {
  return identifier;
}

- (int) getAmount {
  return amount;
}

- (PaymentStatus) getStatus {
  return status;
}

- (NSDictionary *) toResponse {
  NSMutableDictionary *response = [[NSMutableDictionary alloc] init];

  response[@"id"] = identifier;
  response[@"status"] = status;
  response[@"amount"] = [NSNumber numberWithInteger:amount];

  if (transactionId != nil) {
    response[@"transactionId"] = transactionId;
  }

  if (customerId != nil) {
    response[@"customerId"] = customerId;
  }

  if (cardId != nil) {
    response[@"cardId"] = cardId;
  }

  if (maskedPan != nil) {
    response[@"maskedPan"] = maskedPan;
  }

  if (receiptData != nil) {
    response[@"receiptData"] = receiptData;
  }

  return response;
}

@end
