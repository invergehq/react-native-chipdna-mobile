#import <CoreBluetooth/CoreBluetooth.h>
#import <React/RCTLog.h>
#import <Foundation/Foundation.h>

#import "ChipDnaModule.h"
#import "ChipDnaMobile/ChipDnaMobileSerializer.h"

#import "AvailablePinPadsListener.h"
#import "ApplicationSelectionListener.h"
#import "UserNotificationListener.h"
#import "ConnectAndConfigureFinishedListener.h"
#import "TransactionFinishedListener.h"
#import "SignatureVerificationListener.h"
#import "SignatureCaptureListener.h"
#import "PaymentDetails.h"


static id availablePinPadsTarget = nil;
static id connectAndConfigureFinishedTarget = nil;
static id transactionFinishedTarget = nil;
static id cardApplicationSelectionTarget = nil;
static id userNotificationTarget = nil;
static id signatureVerificationTarget = nil;
static id signatureCaptureTarget = nil;

@implementation ChipDnaModule {
  BOOL hasListeners;
}

RCT_EXPORT_MODULE(ChipDNAMobile)

- (void)startObserving {
    hasListeners = YES;
}

- (void)stopObserving {
    hasListeners = NO;
}

- (NSArray<NSString *> *)supportedEvents {
    return @[@"onStatusUpdate", @"onSendMessage"];
}

- (void) statusUpdate:(NSString *)message {
  if (hasListeners) {
    [self sendEventWithName:@"onStatusUpdate" body:message];
  }
}

- (void) sendMessage:(NSString *)message {
  RCTLogInfo(@"RNChipDNAMobile %@", message);
  if (hasListeners) {
    [self sendEventWithName:@"onSendMessage" body:message];
  }
}

- (void) transactionUpdateListener:(CCParameters *)parameters {
  NSString *transactionUpdate = [parameters valueForKey:CCParamTransactionUpdate];
  [self statusUpdate:transactionUpdate];
  [self sendMessage:[NSString stringWithFormat:@"Transaction update: %@", transactionUpdate]];
}

+ (void) addAvailablePinPadsTarget:(id)target action:(SEL)action {
  if (availablePinPadsTarget) {
    [ChipDnaMobile removeAvailablePinPadsTarget:availablePinPadsTarget];
  }
  availablePinPadsTarget = target;
  [ChipDnaMobile addAvailablePinPadsTarget:target action:action];
}

+ (void) addConnectAndConfigureFinishedTarget:(id)target action:(SEL)action {
  if (connectAndConfigureFinishedTarget) {
    [ChipDnaMobile removeConnectAndConfigureFinishedTarget:connectAndConfigureFinishedTarget];
  }
  connectAndConfigureFinishedTarget = target;
  [ChipDnaMobile addConnectAndConfigureFinishedTarget:target action:action];
}

+ (void) addTransactionFinishedTarget:(id)target action:(SEL)action {
  if (transactionFinishedTarget) {
    [ChipDnaMobile removeTransactionFinishedTarget:transactionFinishedTarget];
  }
  transactionFinishedTarget = target;
  [ChipDnaMobile addTransactionFinishedTarget:target action:action];
}

+ (void) addCardApplicationSelectionTarget:(id)target action:(SEL)action {
  if (cardApplicationSelectionTarget) {
    [ChipDnaMobile removeCardApplicationSelectionTarget:cardApplicationSelectionTarget];
  }
  cardApplicationSelectionTarget = target;
  [ChipDnaMobile addCardApplicationSelectionTarget:target action:action];
}

+ (void) addUserNotificationTarget:(id)target action:(SEL)action {
  if (userNotificationTarget) {
    [ChipDnaMobile removeUserNotificationTarget:userNotificationTarget];
  }
  userNotificationTarget = target;
  [ChipDnaMobile addUserNotificationTarget:target action:action];
}

+ (void) addSignatureVerificationTarget:(id)target action:(SEL)action {
  if (signatureVerificationTarget) {
    [ChipDnaMobile removeSignatureVerificationTarget:signatureVerificationTarget];
  }
  signatureVerificationTarget = target;
  [ChipDnaMobile addSignatureVerificationTarget:target action:action];
}

+ (void) addSignatureCaptureTarget:(id)target action:(SEL)action {
  if (signatureCaptureTarget) {
    [ChipDnaMobile removeSignatureCaptureTarget:signatureCaptureTarget];
  }
  signatureCaptureTarget = target;
  [ChipDnaMobile addSignatureCaptureTarget:target action:action];
}

RCT_EXPORT_METHOD(loggedIn:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject)
{
  if ([ChipDnaMobile isInitialized]) {
      CCParameters *statusParameters = [[ChipDnaMobile sharedInstance] getStatus:nil];

      NSString *apiKey = [statusParameters valueForKey:CCParamApiKey];
      [self sendMessage:[NSString stringWithFormat:@"API Key: %@", apiKey]];

      if (apiKey) {
        resolve([NSNumber numberWithBool:TRUE]);
      } else {
        resolve([NSNumber numberWithBool:FALSE]);
      }
  } else {
      [self sendMessage:@"Not initialized"];
      resolve([NSNumber numberWithBool:FALSE]);
  }
}

RCT_EXPORT_METHOD(getDevices:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{  
  if ([ChipDnaMobile isInitialized]) {
    AvailablePinPadsListener *listener = [[AvailablePinPadsListener alloc] initWithResolver:resolve withRejecter:reject withModule:self];

    [ChipDnaModule addAvailablePinPadsTarget:listener action:@selector(availablePinPadsListener:)];
    [[ChipDnaMobile sharedInstance] getAvailablePinPads:nil];
  } else {
    resolve([NSArray array]);
  }
}

RCT_EXPORT_METHOD(login:(NSString *)password 
                  apiKey:(NSString *)apiKey 
                  environment:(NSString *)environment 
                  appId:(NSString *)appId 
                  resolver:(RCTPromiseResolveBlock)resolve 
                  rejecter:(RCTPromiseRejectBlock)reject)
{  
  CCParameters *requestParameters = [[CCParameters alloc] init];

  [requestParameters setValue:apiKey forKey:CCParamApiKey];
  [requestParameters setValue:[appId uppercaseString] forKey:CCParamApplicationIdentifier];

  if ([environment isEqualToString:@"LIVE"]) {
    [requestParameters setValue:CCValueEnvironmentLive forKey:CCParamEnvironment];
  } else {
    [requestParameters setValue:CCValueEnvironmentTest forKey:CCParamEnvironment];
  }

  if ([ChipDnaMobile isInitialized]) {
    [[ChipDnaMobile sharedInstance] setProperties:requestParameters];
    resolve([NSNumber numberWithBool:TRUE]);
  } else {
    CCParameters *initParams = [[CCParameters alloc] init];
    [initParams setValue:password forKey:CCParamPassword];
    CCParameters *response = [ChipDnaMobile initialize:initParams];

    NSString* result = [response valueForKey:CCParamResult];

    if (result && [result caseInsensitiveCompare:@"True"] == NSOrderedSame) {
      RCTLogInfo(@"ChipDna Mobile initialised");

      [requestParameters setValue:CCValueTrue forKey:CCParamDigitalSignatureSupported];
      [[ChipDnaMobile sharedInstance] setProperties:requestParameters];
      [ChipDnaMobile addTransactionUpdateTarget:self action:@selector(transactionUpdateListener:)];

      resolve([NSNumber numberWithBool:TRUE]);
    } else {
      RCTLogInfo(@"Failed to initialise ChipDna Mobile");
      NSString *remainingAttempts = [response valueForKey:CCParamRemainingAttempts];

      if (remainingAttempts && [remainingAttempts isEqualToString:@"0"]) {
        RCTLogInfo(@"Reached password attempt limit");
        reject(@"Password", @"Reached password attempt limit", nil);
      } else {
        RCTLogInfo(@"Password attempts remaining: %@", remainingAttempts);
        reject(@"Password", [NSString stringWithFormat:@"%@ %@", @"Password attempts remaining:", remainingAttempts], nil);
      }
    }
  }
}

RCT_EXPORT_METHOD(connectDevice:(NSDictionary *)device 
                  resolver:(RCTPromiseResolveBlock)resolve 
                  rejecter:(RCTPromiseRejectBlock)reject)
{  
  [self sendMessage:@"Connecting..."];

  NSString *name = [device objectForKey:@"name"];
  NSString *connectionType = [device objectForKey:@"connectionType"];

  if (name && connectionType) {
    CCParameters *requestParameters = [[CCParameters alloc] init];

    [requestParameters setValue:name forKey:CCParamPinPadName];
    [requestParameters setValue:connectionType forKey:CCParamPinPadConnectionType];

    CCParameters *response = [[ChipDnaMobile sharedInstance] setProperties:requestParameters];

    NSString* result = [response valueForKey:CCParamResult];
    if (result && [result caseInsensitiveCompare:@"True"] == NSOrderedSame) {
      ConnectAndConfigureFinishedListener *listener = [[ConnectAndConfigureFinishedListener alloc] initWithResolver:resolve withRejecter:reject];
      [ChipDnaModule addConnectAndConfigureFinishedTarget:listener action:@selector(connectAndConfigureFinishedListener:)];

      ChipDnaMobile *module = [ChipDnaMobile sharedInstance];
      CCParameters *response = [module connectAndConfigure:[module getStatus:nil]];
      NSString *result = [response valueForKey:CCParamResult];

      if (result && [result isEqualToString:CCValueFalse]) {
        NSString *errors = [response valueForKey:CCParamErrors];
        [self sendMessage:[NSString stringWithFormat:@"Error: %@", errors]];
        reject(@"Device", errors, nil);
      }
    } else {
      reject(@"Device", @"Failed connecting", nil);
    }
  } else {
    reject(@"Device", @"Both 'name' and 'connectionType' are required", nil);
  }
}

RCT_EXPORT_METHOD(reconnectDevice:(RCTPromiseResolveBlock)resolve 
                  rejecter:(RCTPromiseRejectBlock)reject) 
{  
  CCParameters *statusParameters = [[ChipDnaMobile sharedInstance] getStatus:nil];

  NSString *pinPadName = [statusParameters valueForKey:CCParamPinPadName];
  NSString *pinPadConnectionType = [statusParameters valueForKey:CCParamPinPadConnectionType];

  if ([pinPadName length] && [pinPadConnectionType length]) {
    CCParameters *requestParameters = [[CCParameters alloc] init];

    [requestParameters setValue:pinPadName forKey:CCParamPinPadName];
    [requestParameters setValue:pinPadConnectionType forKey:CCParamPinPadConnectionType];

    CCParameters *response = [[ChipDnaMobile sharedInstance] setProperties:requestParameters];

    NSString *result = [response valueForKey:CCParamResult];

    if (result && [result isEqualToString:CCValueTrue]) {
      [self sendMessage:@"Reconnected to pin pad"];
      ConnectAndConfigureFinishedListener *listener = [[ConnectAndConfigureFinishedListener alloc] initWithResolver:resolve withRejecter:reject];
      [ChipDnaModule addConnectAndConfigureFinishedTarget:listener action:@selector(connectAndConfigureFinishedListener:)];

      ChipDnaMobile *module = [ChipDnaMobile sharedInstance];
      CCParameters *response = [module connectAndConfigure:[module getStatus:nil]];
      NSString *result = [response valueForKey:CCParamResult];

      if (result && [result isEqualToString:CCValueFalse]) {
        NSString *errors = [response valueForKey:CCParamErrors];
        [self sendMessage:[NSString stringWithFormat:@"Error: %@", errors]];
        reject(@"Device", errors, nil);
      }
    } else {
      reject(@"Device", @"Failed connecting", nil);
    }
  }
}

RCT_EXPORT_METHOD(isDeviceConnected:(RCTPromiseResolveBlock)resolve 
                  rejecter:(RCTPromiseRejectBlock)reject) 
{  
  if ([ChipDnaMobile isInitialized]) {
      CCParameters *statusParameters = [[ChipDnaMobile sharedInstance] getStatus:nil];

      for (id key in [statusParameters allKeys]) {
        NSString *value = [statusParameters valueForKey:key];
        [self sendMessage:[NSString stringWithFormat:@"%@=%@", key, value]];
      }

      NSString *deviceStatus = [statusParameters valueForKey:CCParamDeviceStatus];

      if (deviceStatus) {
        DeviceStatus *status = [ChipDnaMobileSerializer deserializeDeviceStatus:deviceStatus];

        resolve([NSNumber numberWithBool:(status.deviceStatus == DeviceStatusConnected)]);
      }
  } else {
      [self sendMessage:@"Not initialized"];
      resolve([NSNumber numberWithBool:FALSE]);
  }
}

- (void) closePayment:(NSString *)paymentDetailsId 
         amount:(NSString *)amount 
         tip:(NSString *)tip 
         paymentDetails:(PaymentDetails *)paymentDetails
         resolver:(RCTPromiseResolveBlock)resolve
         rejecter:(RCTPromiseRejectBlock)reject
{
  CCParameters *requestParameters = [[CCParameters alloc] init];
  [requestParameters setValue:paymentDetailsId forKey:CCParamUserReference];
  [requestParameters setValue:amount forKey:CCParamAmount];
  if (tip) {
    [requestParameters setValue:tip forKey:CCParamTipAmount];
  }
  [requestParameters setValue:CCValueTrue forKey:CCParamCloseTransaction];

  [self statusUpdate:@"ConfirmTransaction"];
  CCParameters *response = [[ChipDnaMobile sharedInstance] confirmTransaction:requestParameters];

  NSString *transactionResult = [response valueForKey:CCParamTransactionResult];
  if (transactionResult && [transactionResult isEqualToString:CCValueApproved]) {
    resolve([paymentDetails toResponse]);
  } else {
    reject(@"Payment", [NSString stringWithFormat:@"Payment failed %@ %@", [response valueForKey:CCParamResult], [response valueForKey:CCParamErrors]], nil);
  }
}

RCT_EXPORT_METHOD(makePayment:(int)amount 
                  currency:(NSString *)currency
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) 
{  
  PaymentDetails *paymentDetails = [[PaymentDetails alloc] initWithAmount:amount 
                                                           withPaymentStatus:PaymentStatusUnknown];

  TransactionFinishedListener *listener = [[TransactionFinishedListener alloc] initWithResolver:resolve
    withRejecter:reject
    withCallback:^(PaymentDetails *details) {
      NSString *paymentDetailsId = [paymentDetails getId];
      RCTPromiseResolveBlock resolvePromise = resolve;
      RCTPromiseRejectBlock rejectPromise = reject;

      ChipDnaModule *module = self;
      TransactionFinishedListener *listener = [[TransactionFinishedListener alloc] initWithResolver:resolve
        withRejecter:reject
        withCallback:nil
        withModule:self
      ];
      [ChipDnaModule addTransactionFinishedTarget:listener action:@selector(transactionFinishedListener:)];

      [module closePayment:paymentDetailsId
              amount:[NSString stringWithFormat:@"%d", amount]
              tip:nil
              paymentDetails:details
              resolver:resolvePromise
              rejecter:rejectPromise];
    }
    withModule:self
  ];
  [ChipDnaModule addTransactionFinishedTarget:listener action:@selector(transactionFinishedListener:)];

  CCParameters *requestParameters = [[CCParameters alloc] init];
  [requestParameters setValue:[NSString stringWithFormat:@"%d", amount] forKey:CCParamAmount];
  [requestParameters setValue:CCValueAmountTypeActual forKey:CCParamAmountType];
  [requestParameters setValue:currency forKey:CCParamCurrency];
  [requestParameters setValue:[paymentDetails getId] forKey:CCParamUserReference];
  [requestParameters setValue:CCValueSale forKey:CCParamTransactionType];
  [requestParameters setValue:CCValueCard forKey:CCParamPaymentMethod];

  ApplicationSelectionListener *applicationListener = [[ApplicationSelectionListener alloc] initWithModule:self];
  [ChipDnaModule addCardApplicationSelectionTarget:applicationListener action:@selector(applicationSelectionListener:)];

  UserNotificationListener *userNotificationListener = [[UserNotificationListener alloc] initWithModule:self];
  [ChipDnaModule addUserNotificationTarget:userNotificationListener action:@selector(userNotificationListener:)];

  SignatureVerificationListener *signatureVerificationListener = [[SignatureVerificationListener alloc] initWithResolver:resolve withRejecter:reject withPaymentDetails:paymentDetails withCallback:nil withModule:self];
  [ChipDnaModule addSignatureVerificationTarget:signatureVerificationListener action:@selector(signatureVerificationListener:)];

  SignatureCaptureListener *signatureCaptureListener = [[SignatureCaptureListener alloc] initWithResolver:resolve withRejecter:reject withPaymentDetails:paymentDetails withModule:self];
  [ChipDnaModule addSignatureCaptureTarget:signatureCaptureListener action:@selector(signatureCaptureListener:)];

  [self sendMessage:@"Make payment"];

  CCParameters *response = [[ChipDnaMobile sharedInstance] startTransaction:requestParameters];

  NSString *result = [response valueForKey:CCParamResult];

  if (result && [result isEqualToString:CCValueFalse]) {
    NSString *errors = [response valueForKey:CCParamErrors];
    [self sendMessage:[NSString stringWithFormat:@"Error: %@", errors]];
    reject(@"Payment", errors, nil);
  }
}

RCT_EXPORT_METHOD(updateTMS:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) 
{  
  [self sendMessage:@"Update TMS"];
  CCParameters *tmsParams = [[CCParameters alloc] init];

  [tmsParams setValue:CCValueTrue forKey:CCParamForceTmsUpdate];
  [tmsParams setValue:CCValueFalse forKey:CCParamFullTmsUpdate];

  CCParameters *response = [[ChipDnaMobile sharedInstance] requestTmsUpdate:tmsParams];
  NSString *result = [response valueForKey:CCParamResult];

  if (!result || [result isEqualToString:CCValueFalse]) {
    [self sendMessage:@"TMS Update failed"];
    resolve([NSNumber numberWithBool:FALSE]);
  } else {
    [self sendMessage:@"TMS Updated"];
    resolve([NSNumber numberWithBool:TRUE]);
  }
}

RCT_EXPORT_METHOD(cancelVerification:(RCTPromiseResolveBlock)resolve
                rejecter:(RCTPromiseRejectBlock)reject) 
{  
  [self sendMessage:@"Cancel payment"];

  CCParameters *declineParameters = [[CCParameters alloc] init];
  [declineParameters setValue:CCValueFalse forKey:CCParamResult];
  CCParameters *response = [[ChipDnaMobile sharedInstance] continueSignatureVerification:declineParameters];

  NSString *result = [response valueForKey:CCParamResult];

  if (!result || [result isEqualToString:CCValueFalse]) {
    resolve([NSNumber numberWithBool:FALSE]);
  } else {
    resolve([NSNumber numberWithBool:TRUE]);
  }
}

RCT_EXPORT_METHOD(verifyPayment:(NSDictionary *)payment 
                  verification:(NSString *)verification
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) 
{  
  [self sendMessage:@"Verify payment"];

  PaymentDetails* paymentDetails = [[PaymentDetails alloc] initWithDictionary:payment];

  BOOL isPinRequired = [[paymentDetails getStatus] isEqualToString:PaymentStatusPinRequired];
  BOOL isSignatureRequired = [[paymentDetails getStatus] isEqualToString:PaymentStatusSignatureRequired];
  if (isPinRequired || (verification && [verification length] == 0 && isSignatureRequired)) {
    CCParameters *parameters = [[CCParameters alloc] init];
    [parameters setValue:CCValueTrue forKey:CCParamResult];
    [parameters setValue:verification forKey:CCParamOperatorPin];

    SignatureVerificationListener *signatureVerificationListener = [[SignatureVerificationListener alloc] initWithResolver:resolve withRejecter:reject 
      withPaymentDetails:paymentDetails
      withCallback:^{
        [self sendMessage:@"verifyPayment: Signature verification callback"];
        CCParameters *requestParameters = [[CCParameters alloc] init];
        [requestParameters setValue:CCValueTrue forKey:CCParamResult];

        CCParameters *response = [[ChipDnaMobile sharedInstance] continueSignatureVerification:requestParameters];

        NSString *result = [response valueForKey:CCParamResult];

        if (result && [result isEqualToString:CCValueFalse]) {
          [self sendMessage:@"verifyPayment: continueSignatureVerification failed"];
        } else {
          [self sendMessage:@"verifyPayment: continueSignatureVerification success"];
        }
      }
      withModule:self
    ];
    [ChipDnaModule addSignatureVerificationTarget:signatureVerificationListener action:@selector(signatureVerificationListener:)];

    TransactionFinishedListener *listener = [[TransactionFinishedListener alloc] initWithResolver:resolve
      withRejecter:reject
      withCallback:^(PaymentDetails *details) {
        RCTPromiseResolveBlock resolvePromise = resolve;
        RCTPromiseRejectBlock rejectPromise = reject;

        TransactionFinishedListener *listener = [[TransactionFinishedListener alloc] initWithResolver:resolve
          withRejecter:reject
          withCallback:nil
          withModule:self
        ];
        [ChipDnaModule addTransactionFinishedTarget:listener action:@selector(transactionFinishedListener:)];

        [self closePayment:[paymentDetails getId] 
              amount:[@([paymentDetails getAmount]) stringValue]
              tip:nil
              paymentDetails:details
              resolver:resolvePromise
              rejecter:rejectPromise];
      }
      withModule:self
    ];
    [ChipDnaModule addTransactionFinishedTarget:listener action:@selector(transactionFinishedListener:)];
    [[ChipDnaMobile sharedInstance] continueSignatureVerification:parameters];
  } else if (isSignatureRequired) {
    [self sendMessage:@"Do signature verification"];

    SignatureVerificationListener *signatureVerificationListener = [[SignatureVerificationListener alloc] initWithResolver:resolve withRejecter:reject 
      withPaymentDetails:paymentDetails
      withCallback:^{
        [self sendMessage:@"verifyPayment: Signature verification callback"];
        CCParameters *requestParameters = [[CCParameters alloc] init];
        [requestParameters setValue:CCValueTrue forKey:CCParamResult];

        CCParameters *response = [[ChipDnaMobile sharedInstance] continueSignatureVerification:requestParameters];

        NSString *result = [response valueForKey:CCParamResult];

        if (result && [result isEqualToString:CCValueFalse]) {
          [self sendMessage:@"verifyPayment: continueSignatureVerification failed"];
        } else {
          [self sendMessage:@"verifyPayment: continueSignatureVerification success"];
        }
      }
      withModule:self
    ];
    [ChipDnaModule addSignatureVerificationTarget:signatureVerificationListener action:@selector(signatureVerificationListener:)];

    SignatureCaptureListener *signatureCaptureListener = [[SignatureCaptureListener alloc] initWithResolver:resolve withRejecter:reject withPaymentDetails:paymentDetails withModule:self];
    [ChipDnaModule addSignatureCaptureTarget:signatureCaptureListener action:@selector(signatureCaptureListener:)];

    TransactionFinishedListener *listener = [[TransactionFinishedListener alloc] initWithResolver:resolve
      withRejecter:reject
      withCallback:^(PaymentDetails *details) {
        [self sendMessage:@"Signature verified and capture"];
        [self closePayment:[details getId] 
              amount:[@([details getAmount]) stringValue]
              tip:nil
              paymentDetails:details
              resolver:resolve
              rejecter:reject];
      }
      withModule:self
    ];
    [ChipDnaModule addTransactionFinishedTarget:listener action:@selector(transactionFinishedListener:)];

    CCParameters *requestParameters = [[CCParameters alloc] init];
    [requestParameters setValue:verification forKey:CCParamSignatureData];

    CCParameters *response = [[ChipDnaMobile sharedInstance] continueSignatureCapture:requestParameters];

    NSString *result = [response valueForKey:CCParamResult];
    if (result && [result isEqualToString:CCValueTrue]) {
      [self sendMessage:@"Capture success"];
      // FIXME: Should we resolve the promise here?
    } else {
      reject(@"Signature", [NSString stringWithFormat:@"%@ %@", @"Signature failed due to", [response valueForKey:CCParamErrors]], nil);
    }
  } else {
    reject(@"Verification", @"Invalid verification data sent", nil);
  }
}

@end
