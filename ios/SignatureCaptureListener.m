#import <React/RCTLog.h>
#import "SignatureCaptureListener.h"

@implementation SignatureCaptureListener  {
  RCTPromiseResolveBlock resolve;
  RCTPromiseRejectBlock reject;
  PaymentDetails *paymentDetails;
  ChipDnaModule *module;
}

- (id) initWithResolver:(RCTPromiseResolveBlock)resolver 
       withRejecter:(RCTPromiseRejectBlock)rejecter 
       withPaymentDetails:(PaymentDetails *)details
       withModule:(ChipDnaModule *)m
{
  if (self = [super init]) {
    resolve = resolver;
    reject = rejecter;
    paymentDetails = details;
    module = m;
  }

  return self;
}

- (void) signatureCaptureListener:(CCParameters *)parameters {
  [module sendMessage:@"Signature captured"];

  NSString *responseRequired = [parameters valueForKey:CCParamResponseRequired];
  NSString *operatorPinRequired = [parameters valueForKey:CCParamOperatorPinRequired];

  if ([responseRequired isEqualToString:CCValueTrue]) {
      [module sendMessage:@"Signature Check Required"];
      PaymentDetails *details = nil;
      if ([operatorPinRequired isEqualToString:CCValueTrue]) {
        details = [[PaymentDetails alloc] initWithPaymentDetails:paymentDetails withPaymentStatus:PaymentStatusPinRequired];
      } else {
        details = [[PaymentDetails alloc] initWithPaymentDetails:paymentDetails withPaymentStatus:PaymentStatusSignatureRequired];
      }
      resolve([details toResponse]);
  } else {
      /* callback */
  }
}

@end
