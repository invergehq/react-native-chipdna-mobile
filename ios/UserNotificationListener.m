#import <React/RCTLog.h>
#import "UserNotificationListener.h"

@implementation UserNotificationListener {
  ChipDnaModule *module;
}

- (id) initWithModule:(ChipDnaModule *)m
{
  if (self = [super init]) {
    module = m;
  }

  return self;
}


- (void) userNotificationListener:(CCParameters *)parameters {
    NSString *responseRequired = [parameters valueForKey:CCParamResponseRequired];

    if ([responseRequired isEqualToString:CCValueTrue]) {
      [module sendMessage:@"User notification response required"];
    } else {
      [module sendMessage:@"User notification response not required"];
    }
}

@end
