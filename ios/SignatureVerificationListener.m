#import <React/RCTLog.h>
#import "SignatureVerificationListener.h"

@implementation SignatureVerificationListener  {
  RCTPromiseResolveBlock resolve;
  RCTPromiseRejectBlock reject;
  PaymentDetails *paymentDetails;
  ChipDnaModule *module;
  void(^callback)();
}

- (id) initWithResolver:(RCTPromiseResolveBlock)resolver 
       withRejecter:(RCTPromiseRejectBlock)rejecter 
       withPaymentDetails:(PaymentDetails *)paymentDetails
       withCallback:(void(^)())callbackFunction
       withModule:(ChipDnaModule *)m
{
  if (self = [super init]) {
    resolve = resolver;
    reject = rejecter;
    paymentDetails = paymentDetails;
    callback = callbackFunction;
    module = m;
  }

  return self;
}

- (void) signatureVerificationListener:(CCParameters *)parameters {
  NSString *responseRequired = [parameters valueForKey:CCParamResponseRequired];
  NSString *operatorPinRequired = [parameters valueForKey:CCParamOperatorPinRequired];
  NSString *digitalSignatureSupported = [parameters valueForKey:CCParamDigitalSignatureSupported];

  if ([responseRequired isEqualToString:CCValueTrue]) {
      [module sendMessage:@"Signature Check Required"];
      PaymentDetails *details = nil;
      if ([operatorPinRequired isEqualToString:CCValueTrue]) {
        [module sendMessage:@"PIN Required"];
        details = [[PaymentDetails alloc] initWithPaymentDetails:paymentDetails withPaymentStatus:PaymentStatusPinRequired];
        resolve([details toResponse]);
      } else if ([digitalSignatureSupported isEqualToString:CCValueTrue]) {
        [module sendMessage:@"Digital Signature Required"];
        details = [[PaymentDetails alloc] initWithPaymentDetails:paymentDetails withPaymentStatus:PaymentStatusSignatureRequired];
        resolve([details toResponse]);
      } else {
        [module sendMessage:@"Physical Signature Required"];
        CCParameters *requestParameters = [[CCParameters alloc] init];
        [requestParameters setValue:CCValueTrue forKey:CCParamResult];

        CCParameters *response = [[ChipDnaMobile sharedInstance] continueSignatureVerification:requestParameters];

        NSString *result = [response valueForKey:CCParamResult];

        if (result && [result isEqualToString:CCValueFalse]) {
          [module sendMessage:@"signatureVerificationListener: continueSignatureVerification failed"];
        } else {
          [module sendMessage:@"signatureVerificationListener: continueSignatureVerification success"];
        }
      }
  } else {
      [module sendMessage:@"Signature Check On Keypad"];
      if (callback) {
        callback();
      }
  }
}

@end
